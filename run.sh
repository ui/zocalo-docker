#!/bin/bash

. /opt/conda/etc/profile.d/conda.sh
conda activate zocalo

set -e

if [ -t 0 ] ; then
    supervisord &

    CMD="/bin/bash -c . /opt/conda/etc/profile.d/conda.sh && conda activate zocalo"
    if [ "$1" ]
    then
        CMD=$@
    fi

    exec $CMD
else
    exec supervisord
fi
