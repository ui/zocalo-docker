#!/bin/bash
docker run --name zocalo \
    --link mariadb \
    -v $(pwd)/../zocalo-esrf:/zocalo/python-zocalo-esrf \
    -v $(pwd)/../zocalo-recipes:/zocalo/zocalo-recipes \
    -v /Users/Shared/data:/data \
    -v /Users/Shared/zocalo-test:/zocalo-test \
    -p 9032:9032 \
    esrfbcu/zocalo:main
