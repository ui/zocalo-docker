# docker build -t zocalo

# Intermediate to do checkout
FROM alpine/git:v2.32.0 AS checkout
 
# Clone repos
WORKDIR /zocalo

RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh && \
    ssh-keyscan gitlab.esrf.fr > /root/.ssh/known_hosts && \
    ssh-keyscan github.com >> /root/.ssh/known_hosts

RUN git clone https://github.com/stufisher/python-zocalo && \
    git clone https://gitlab.esrf.fr/workflow/python-zocalo-esrf.git && \
    git clone https://gitlab.esrf.fr/workflow/zocalo-recipes.git && \
    git clone https://github.com/woutdenolf/spectrocrunch

# Actual image
FROM continuumio/miniconda3:4.10.3 AS main
COPY --from=checkout /zocalo/ /zocalo/
COPY config/ /zocalo/config/

# Set timezone
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install build essential
RUN apt-get update && \
    apt-get install -y --no-install-recommends build-essential && \
    apt-get clean autoclean && apt-get autoremove --yes && \
    rm -rf /var/lib/apt/lists/*

# Setup environment
RUN conda config --prepend channels conda-forge
RUN conda update --yes --all && \
    conda create --yes --name zocalo && \
    conda clean --all

RUN ["/bin/bash", "-c", ". /opt/conda/etc/profile.d/conda.sh && \
    pip install supervisor multivisor[web]"]
COPY supervisor/supervisord.conf /etc/supervisor/supervisord.conf

WORKDIR /zocalo
SHELL ["conda", "run", "-n", "zocalo", "/bin/bash", "-c"]
RUN conda install python=3.9 pymca && conda clean --all
RUN cd python-zocalo && git checkout main-external && pip install -e . && cd .. && \
    cd spectrocrunch && pip install -e . && cd .. && \
    cd python-zocalo-esrf && pip install -e . && cd .. && \
    pip install --no-cache-dir lmfit==1.0.2 xraylarch==0.9.50 && \
    conda clean --all && pip cache purge 

RUN echo "conda activate zocalo" >> ~/.bashrc
COPY conda/env_vars.sh /opt/conda/envs/zocalo/etc/conda/activate.d/

# Run
WORKDIR /zocalo
ENTRYPOINT ["/run.sh"]
COPY run.sh /

EXPOSE 9032
