# Building

```bash
docker build --no-cache -t zocalo:<release> .
```

# Running

Start the preconfigured activemq instance. Grab the zocalo-activemq image from https://gitlab.esrf.fr/workflows/zocalo-docker-activemq
```bash
docker run --name activemq -p 8161:8161 -p 61613:61613 esrfbcu/zocalo-activemq:main
```

Start the test database. Grab the daiquiri-testdb image from https://gitlab.esrf.fr/ui/daiquiri-docker-testdb
```bash
docker run --name mariadb esrfbcu/daiquiri-testdb:1.27
```

Start the zocalo docker:

```bash
docker run --link mariadb --link activemq -p 9032:9032 esrfbcu/zocalo:main
```

It expects to find activemq running on localhost:61613

### Ports

The following ports can be mapped

Port | Service
---- | ---
9032 | Supervisor
9033 | Multivisor RPC
